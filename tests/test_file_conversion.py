from pathlib import Path

import numpy as np

from run_spikesorting.file_conversion import blackrock_channel_names, convert_blackrock_to_kilosort

path_to_data = (Path('tests') / 'test_data')

from pytest import mark

test_header = {'nb_block': 1,
               'nb_segment': [1],
               'signal_channels': np.array([('chan1', 1, 1000., 'int16', 'uV', 0.25, 0., 2),
                                            ('chan2', 2, 1000., 'int16', 'uV', 0.25, 0., 2),
                                            ('ainp9', 73, 1000., 'int16', 'mV', 0.15259255, 0., 2),
                                            ('chan1', 1, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan2', 2, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan3', 3, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan4', 4, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan5', 5, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan6', 6, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan7', 7, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan8', 8, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan9', 9, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan10', 10, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan11', 11, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan12', 12, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan13', 13, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan14', 14, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan15', 15, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan16', 16, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan17', 17, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan18', 18, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan19', 19, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan20', 20, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan21', 21, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan22', 22, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan23', 23, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan24', 24, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan25', 25, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan26', 26, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan27', 27, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan28', 28, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan29', 29, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan30', 30, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan31', 31, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan32', 32, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan33', 33, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan34', 34, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan35', 35, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan36', 36, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan37', 37, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan38', 38, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('chan39', 39, 30000., 'int16', 'uV', 0.25, 0., 6),
                                            ('ainp5', 69, 30000., 'int16', 'mV', 0.15259255, 0., 6),
                                            ('ainp6', 70, 30000., 'int16', 'mV', 0.15259255, 0., 6),
                                            ('ainp7', 71, 30000., 'int16', 'mV', 0.15259255, 0., 6),
                                            ('ainp8', 72, 30000., 'int16', 'mV', 0.15259255, 0., 6),
                                            ('ainp9', 73, 30000., 'int16', 'mV', 0.15259255, 0., 6),
                                            ('ainp10', 74, 30000., 'int16', 'mV', 0.15259255, 0., 6)],
                                           dtype=[('name', '<U64'), ('id', '<i8'), ('sampling_rate', '<f8'),
                                                  ('dtype', '<U16'), ('units', '<U64'), ('gain', '<f8'),
                                                  ('offset', '<f8'), ('group_id', '<i8')]),
               'unit_channels': np.array([('ch1#0', 'Unit 1000', 'uV', 0.25, 0., 10, 30000.),
                                          ('ch2#0', 'Unit 2000', 'uV', 0.25, 0., 10, 30000.),
                                          ('ch3#0', 'Unit 3000', 'uV', 0.25, 0., 10, 30000.),
                                          ('ch4#0', 'Unit 4000', 'uV', 0.25, 0., 10, 30000.),
                                          ('ch5#0', 'Unit 5000', 'uV', 0.25, 0., 10, 30000.),
                                          ('ch6#0', 'Unit 6000', 'uV', 0.25, 0., 10, 30000.),
                                          ('ch7#0', 'Unit 7000', 'uV', 0.25, 0., 10, 30000.),
                                          ('ch8#0', 'Unit 8000', 'uV', 0.25, 0., 10, 30000.),
                                          ('ch9#0', 'Unit 9000', 'uV', 0.25, 0., 10, 30000.),
                                          ('ch11#0', 'Unit 11000', 'uV', 0.25, 0., 10, 30000.),
                                          ('ch12#0', 'Unit 12000', 'uV', 0.25, 0., 10, 30000.),
                                          ('ch13#0', 'Unit 13000', 'uV', 0.25, 0., 10, 30000.),
                                          ('ch14#0', 'Unit 14000', 'uV', 0.25, 0., 10, 30000.),
                                          ('ch15#0', 'Unit 15000', 'uV', 0.25, 0., 10, 30000.),
                                          ('ch16#0', 'Unit 16000', 'uV', 0.25, 0., 10, 30000.),
                                          ('ch17#0', 'Unit 17000', 'uV', 0.25, 0., 10, 30000.),
                                          ('ch18#0', 'Unit 18000', 'uV', 0.25, 0., 10, 30000.),
                                          ('ch19#0', 'Unit 19000', 'uV', 0.25, 0., 10, 30000.),
                                          ('ch20#0', 'Unit 20000', 'uV', 0.25, 0., 10, 30000.),
                                          ('ch21#0', 'Unit 21000', 'uV', 0.25, 0., 10, 30000.),
                                          ('ch22#0', 'Unit 22000', 'uV', 0.25, 0., 10, 30000.),
                                          ('ch23#0', 'Unit 23000', 'uV', 0.25, 0., 10, 30000.),
                                          ('ch24#0', 'Unit 24000', 'uV', 0.25, 0., 10, 30000.),
                                          ('ch25#0', 'Unit 25000', 'uV', 0.25, 0., 10, 30000.),
                                          ('ch26#0', 'Unit 26000', 'uV', 0.25, 0., 10, 30000.),
                                          ('ch27#0', 'Unit 27000', 'uV', 0.25, 0., 10, 30000.),
                                          ('ch28#0', 'Unit 28000', 'uV', 0.25, 0., 10, 30000.),
                                          ('ch29#0', 'Unit 29000', 'uV', 0.25, 0., 10, 30000.),
                                          ('ch30#0', 'Unit 30000', 'uV', 0.25, 0., 10, 30000.),
                                          ('ch31#0', 'Unit 31000', 'uV', 0.25, 0., 10, 30000.),
                                          ('ch32#0', 'Unit 32000', 'uV', 0.25, 0., 10, 30000.)],
                                         dtype=[('name', '<U64'), ('id', '<U64'), ('wf_units', '<U64'),
                                                ('wf_gain', '<f8'), ('wf_offset', '<f8'), ('wf_left_sweep', '<i8'),
                                                ('wf_sampling_rate', '<f8')]),
               'event_channels': np.array([('digital_input_port', '', b'event'),
                                           ('serial_input_port', '', b'event'), ('comments', '', b'event')],
                                          dtype=[('name', '<U64'), ('id', '<U64'), ('type', 'S5')])}


def _check_common_characteristics(nsxheader, channel_indexes):
    """
    Usefull for few IOs (TdtrawIO, NeuroExplorerRawIO, ...).

    Check is a set a signal channel_indexes share common
    characteristics (**sampling_rate/t_start/size**)
    Usefull only when RawIO propose differents channels groups
    with differents sampling_rate for instance.
    """
    # ~ print('_check_common_characteristics', channel_indexes)

    _common_sig_characteristics = ['sampling_rate', 'dtype', 'group_id']
    assert channel_indexes is not None, \
        'You must specify channel_indexes'
    characteristics = nsxheader['signal_channels'][_common_sig_characteristics]
    # ~ print(characteristics[channel_indexes])
    assert np.unique(characteristics[channel_indexes]).size == 1, \
        'This channel set have differents characteristics'


def test_blackrock_raw_indices():
    # asserts are included in function
    channel_indexes = blackrock_channel_names(test_header, n_channels=32)
    _check_common_characteristics(test_header, channel_indexes)


@mark.parametrize('fname', list(path_to_data.glob('*.ns*')))
def test_convert(fname):
    convert_blackrock_to_kilosort(
        file_name=fname.stem,
        input_path=path_to_data,
        kilosort_path=path_to_data,
        channels=range(32)
    )
    assert (path_to_data / fname.stem).exists(), 'kilosort folder was not created'
    assert (path_to_data / fname.stem / 'data.dat').exists(), 'kilosort .dat was not created'
