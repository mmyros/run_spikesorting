from pathlib import Path
from pytest import mark

from run_spikesorting.curation import run_phy

path_to_data = (Path('tests') / 'test_data')


@mark.needs_gpu
@mark.parametrize('fname', list(path_to_data.glob('*.ns*')))
def test_run_phy(fname):
    dat_path = path_to_data / fname.stem / 'data.dat'
    assert dat_path.exists(), f'data.dat {dat_path} did not get created during conversion? cant sort'
    print(dat_path.parent)
    run_phy(path=dat_path.parent)
