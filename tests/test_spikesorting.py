import os
from pathlib import Path

from pytest import mark

path_to_data = (Path('tests') / 'test_data')


@mark.needs_gpu
@mark.parametrize('fname', list(path_to_data.glob('*.ns*')))
def test_run_ksort(fname):
    from run_spikesorting.spikesorting import run_ksort
    print(f'Current directory {Path(os.curdir).absolute()}')
    dat_path = path_to_data / fname.stem / 'data.dat'
    assert dat_path.exists(), f'data.dat {dat_path} did not get created during conversion? cant sort'
    try:
        run_ksort(spikesorting_path=path_to_data / fname.stem)
    except ValueError as e:
        assert 'Not enough spikes' in str(e), f'Not the error we expected: {e}'
