from setuptools import setup, find_packages

# with open('requirements.txt') as f:
#     requirements = f.read().splitlines()

setup(
    name='run_spikesorting',
    version='0.0.1',
    install_requires=[
        'click',
        'neo==0.9.0',
        #'spikeextractors==0.8.*',
        #
        # 'ssm @ git+ssh://git@github.com/mmyros/ssm.git#egg=ssm',
        # 'phylib @ git+ssh://git@github.com/cortex-lab/phylib.git#egg=phylib',
        # 'phy @ git+ssh://git@github.com/cortex-lab/phy.git#egg=phy',
        # 'pykilosort @ git+ssh://git@github.com/mmyros/pykilosort.git#egg=pykilosort',
    ],
    packages=find_packages(),
    include_package_data=True,
    py_modules=['run_spikesorting'],
    entry_points='''
        [console_scripts]
        run_spikesorting=run_spikesorting.scripts.run_spikesorting:cli
    ''',
    url='https://gitlab.com/INsection/run_spikesorting.git',
    license='MIT',
    author='Maxym Myroshnychenko',
    author_email='mmyros@gmail.com',
    description='Workflow to convert and sort extracellular ephys data'
)
