import os
from pathlib import Path


def run_phy(path):
    """
    Python alternative to changing into directory and running:
    phy template-gui params.py
    """
    from phy.apps.template import template_gui
    import time
    # Check if the folder has been spikesorted
    params_path = path / "output" / "params.py"
    if not params_path.exists():
        print(f'Cant run phy on this folder {path}: it does not appear to be spikesorted!')
        time.sleep(1)
        return
    original_working_directory = Path(os.curdir).absolute()
    os.chdir(path / "output")
    template_gui("params.py")
    os.chdir(original_working_directory)
    # template_gui(params_path)
