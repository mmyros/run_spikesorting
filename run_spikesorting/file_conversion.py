import os
import shutil
from os.path import expanduser as eu
from pathlib import Path
import numpy as np
import tqdm
from neo.rawio import BlackrockRawIO


def blackrock_channel_names(nsxheader, n_channels=None, verbose=True):
    import re
    import warnings
    channel_names = np.array([ch['name'] for ch in nsxheader['signal_channels']])
    channels_fs = np.array([ch['sampling_rate'] for ch in nsxheader['signal_channels']])
    channel_numbers = np.array([int(re.sub(r'[^\d]+', '', name)) for name in channel_names])
    channel_names = np.array([i for i, (chan_fs, chan_name) in enumerate(zip(channels_fs, channel_names))
                                if (chan_fs == 30000.) and ('chan' in chan_name)])
    if verbose:
        print(f'Blackrock channels: \n Raw names are {channel_names}\n Fs are {channels_fs} ')
        if n_channels is not None:
            print(f'Taking channels: \n Raw names are {channel_names[channel_names[:n_channels]].T}\n ')
        else:
            print(f'Taking channels: \n Raw names are {channel_names.T}\n ')
    # error checks
    assert (channels_fs[channel_names] == 30000).all(), 'Some channels are not sampled at 30kHz!'
    if not (np.diff(channel_numbers[channel_names]) == 1).all():
        warnings.warn('Were some channels skipped? See above')
    assert (np.diff(channel_numbers[channel_names]) > 0).all(), 'Some channels are not increasing. is this right?'
    if n_channels is not None:
        return channel_names[:n_channels]
    else:
        return channel_names


def check_size(full_blackrock_fname,
               full_kilosort_fname,
               channels):
    from phylib.io.traces import get_ephys_reader as phy_reader
    blackrock_nsx = BlackrockRawIO(str(full_blackrock_fname))
    blackrock_nsx.parse_header()

    blacrock_size = blackrock_nsx.get_signal_size(0, 0)
    # blacrock_size = blackrock_nsx.get_signal_size(0, 0, channel_indexes=channels)  # Old verion of neo

    # Check size
    print(f"Checking phylib using channels {channels}")
    phy_data = phy_reader(str(full_kilosort_fname), n_channels=len(channels), sample_rate=3e4, dtype='int16')
    phy_reader(str(full_kilosort_fname), n_channels=len(channels) +7, sample_rate=3e4, dtype='int16').shape
    print(f'Raw data shape: {phy_data.shape}, should be: {blacrock_size}')
    print(f'Duration: {phy_data.shape[0] / 3e4}, should be: {blacrock_size / 3e4}, ratio {phy_data.shape[0] / blacrock_size}')

    if phy_data.shape[0] != blacrock_size:
        channel_names = blackrock_channel_names(nsxheader=blackrock_nsx.header, n_channels=len(channels))[channels]
        raise IndexError('Check your inputs in channels to process: For instance, are you asking us to convert ainp?'
                         f'{channel_names}')
    else:
        print(f'Succeeded with {full_kilosort_fname}')
    return blacrock_size / 3e4
#
# def check_size(full_blackrock_fname,
#                full_kilosort_fname,
#                n_channels):
#     from phylib.io.traces import get_ephys_reader
#     nsx = BlackrockRawIO(str(full_blackrock_fname))
#     nsx.parse_header()
#
#     channel_indexes = blackrock_raw_indices(nsxheader=nsx.header, n_channels=n_channels)
#     # Only take requested channels: (discard accelerometer for instance)
#     channel_indexes = channel_indexes[: n_channels]
#     size = nsx.get_signal_size(0, 0)
#     # size = nsx.get_signal_size(0, 0, channel_indexes=channel_indexes)  # Old verion of neo?
#
#     # Check size
#     raw_data = get_ephys_reader(str(full_kilosort_fname), n_channels=n_channels, sample_rate=3e4, dtype='int16')
#
#     print(f'Raw data shape: {raw_data.shape}, should be: {size}')
#     print(f'Duration: {raw_data.shape[0] / 3e4}, should be: {size / 3e4}, ratio {raw_data.shape[0] / size}')
#
#     if raw_data.shape[0] != size:
#         raise IndexError('Check your inputs in channels to process: For instance, are you asking us to convert ainp?'
#                          f'{channel_indexes}')
#     else:
#         print(f'Succeeded with {full_kilosort_fname}')
#     return size / 3e4


def convert_blackrock_to_kilosort(file_name,
                                  input_path,
                                  kilosort_path="~/ksort/",
                                  channels=None,
                                  n_channels=None,
                                  **kwargs  # To catch kwargs related to sorting
                                  ):
    """
    # Eg:
    from spikesort.run_spikesorting import file_conversion
    file_conversion.convert_blackrock_to_kilosort(
            kilosort_path = "~/ksort/",
            input_path = "~/data/tmax",
            file_name = "bucket_3_m026_1569338163_")

    # Eg:
    kilosort_path = "~/ksort/"
    input_path = "~/data/tmax"
    file_name = "bucket_3_m026_1569338163_"

    :param file_name: with or without extension, eg "bucket_3_m026_1569338163_" or "bucket_3_m026_1569338163_.ns6"
    :type file_name: Path or str
    :param input_path: Path to file_name
    :type input_path: Path or str
    :param kilosort_path:
    :type kilosort_path:
    :param channels: 32 for 32-ch probe. By default, will use all signal channels, including accelerometer!
    :type channels: iterable
    :return: Path to dat
    :rtype: str
    """

    BUFFERSIZE = 1e8

    # Convert strings to paths for convenience:
    full_blackrock_fname = Path(input_path / file_name).expanduser()

    # If filename lacks an extension, it will be filled in here
    if full_blackrock_fname.with_suffix('.ns5').exists():
        full_blackrock_fname = full_blackrock_fname.with_suffix('.ns5')
    elif full_blackrock_fname.with_suffix('.ns6').exists():
        full_blackrock_fname = full_blackrock_fname.with_suffix('.ns6')

    if not full_blackrock_fname.exists():
        raise FileNotFoundError(f'Did not find blackrock file {full_blackrock_fname}')

    kilosort_path = Path(kilosort_path).expanduser()
    kilosort_path.mkdir(exist_ok=True)
    full_kilosort_fname = kilosort_path / file_name / 'data.dat'
    print(f'Converting {file_name} to {full_kilosort_fname}')

    # Initialize reading from blackrock
    nsx = BlackrockRawIO(str(full_blackrock_fname))
    nsx.parse_header()

    channel_indexes = blackrock_channel_names(nsxheader=nsx.header, n_channels=n_channels)
    # Only take requested channels: (discard accelerometer for instance)
    channel_indexes = channel_indexes[: n_channels]
    # size = nsx.get_signal_size(0, 0, channel_indexes=channel_indexes)
    size = nsx.get_signal_size(0, 0)
    # Handle channels
    if channels is None:
        if n_channels is None:
            # This doesnt include analog input, just signal channels. It does however include accelerometer
            channels = np.arange(nsx.signal_channels_count())
        else:
            channels = np.arange(n_channels)
    else:
        channels = np.array(channels).astype(int)

    n_rows = int(BUFFERSIZE / len(channels))
    print(f'True size is {size}, True duration is {size / 3e4}, channels are {channels}')

    # Initalize .dat
    full_kilosort_fname.parent.mkdir(exist_ok=True)

    # Erase .dat file:
    full_kilosort_fname.unlink(missing_ok=True)

    # Blackrock's provided library:
    # Extract data - note: data will be returned based on *SORTED* elec_ids, see cont_data['elec_ids']
    # THIS IS TOO SLOW: it seeks to the location, which is dumb
    # cont_data = nsx_file.getdata(elec_ids=range(1,max(channels)+2),
    #                              start_time_s=0.003400+index/fs ,data_time_s=(index + n_rows)/fs,
    #                              do_write_dat=False, outfile='/home/myroshnychenkm2/res/test2/data.dat')
    #                              #cont_data['data_headers'][0]['NumDataPoints']
    # buf=cont_data['data'][channels,]

    fdat = open(str(full_kilosort_fname), "w+b")
    cshape = 0
    # Loop over chunks of data
    for i in tqdm.trange(int(size / n_rows) + 1, desc=f'Converting to ks: {full_kilosort_fname}'):
        index = i * n_rows
        cont_data = nsx.get_analogsignal_chunk(i_start=index,
                                               i_stop=index + n_rows,
                                               channel_indexes=channels)

        # Write to .dat
        fdat.write(cont_data.tobytes())
        cshape += cont_data.shape[0]
    fdat.close()
    assert cshape == size
    check_size(full_blackrock_fname,
               full_kilosort_fname,
               channels)

    return str(full_kilosort_fname)


# This uses neo's direct implementation. However, their lazy loading is not working for conversion so forget it
# def convert_to_kilosort(file_name, input_path, output_kilosort_path="~/ksort/"):
#     from neo import Block
#     # Binary lazy may only be supported in neo>8. This will break loading for LFPs
#     # But lests check if lazy checking works in neo=8 first
#
#
#     from neo.io import RawBinarySignalIO, BlackrockIO
#     # import neo
#     from os.path import expanduser as eu
#
#
#     from pathlib import Path
#     output_kilosort_path=eu("~/ksort/")
#
#     input_path = eu("~/data/tmax")
#     file_name = "bucket_3_m026_1569338163_.ns6"
#
#     output_kilosort_path=eu(output_kilosort_path)
#     full_path_blackrock=str(Path(input_path) / file_name)
#     # nsx=neo.rawio.BlackrockRawIO(full_path_blackrock)
#
#     # r = BlackrockIO(filename=full_path_blackrock)#, lazy=True)
#     r = BlackrockIO(filename=full_path_blackrock)
#     w = RawBinarySignalIO(filename=str(output_kilosort_path))#, lazy=True)
#
#     # blocks = r.read()
#     # w.write(blocks[0])
#
#     seg=r.read_segment(lazy=True)
#     w.write(seg)
#     anasig = seg.analogsignals[1].load(time_slice=None, channel_indexes=[0, 2, 18])
#     # w.write(seg.analogsignals[1])
