import shutil
import os
from os.path import expanduser as eu
from pathlib import Path

import numpy as np


def make_trode_probe(dir_path, n_channels=32, bad_channels=(1,), chans_per_trode=2):
    """
    - chanMap - the channel map, specifying which row of the data file
                contains each recorded and connected channel
    - xcoords - a vector the same size as channel map, the physical
                position of each channel on the first dimension
    - ycoords - a vector the same size as channel map, the physical
                position of each channel on the second dimension
    - kcoords - an integer vector the same size as
                chanMap, giving the group number for each channel
    :param dir_path:
    :param n_channels:
    :param bad_channels:
    :param chans_per_trode:
    :return:
    """
    # Prepare index of good channels
    from pykilosort import Bunch
    bad_channels = np.array(bad_channels).astype(int)
    good_channels = np.ones(n_channels).astype(bool)
    if bad_channels.shape[0] > 0:
        good_channels[bad_channels] = False

    # Make channel coordinates and kick out bad channels. Save to dir_path
    np.save(dir_path / 'chanMap.npy',
            np.arange(n_channels)[good_channels])
    np.save(dir_path / 'xc.npy',
            # np.ones(n_channels)[good_channels])
            np.tile(np.linspace(0, .1, chans_per_trode), int(n_channels / chans_per_trode))[good_channels]
            )
    # Stereotrodes are 40 um apart:
    np.save(dir_path / 'yc.npy',
            np.arange(n_channels)[good_channels] * 20
            )

    # No grouping:
    # chan_coords = np.ones(n_channels)[good_channels]
    # trode-specific groups:
    # TODO this needs to be verified
    # I don't think this gets used anywhere actually
    trode_identities = np.vstack(
        [np.tile(np.arange(0, n_channels / chans_per_trode - 1, 2), [chans_per_trode, 1]).T.flatten(),
         np.tile(np.arange(1, n_channels / chans_per_trode, 2), [chans_per_trode, 1]).T.flatten(), ]).T.flatten()
    chan_coords = trode_identities * 40  # Trodes are 40 um apart; channels within trodes are in the same location

    np.save(dir_path / 'kcoords.npy', chan_coords[good_channels])

    # Load probe we just saved
    probe = Bunch()
    probe.NchanTOT = n_channels
    probe.chanMap = np.load(dir_path / 'chanMap.npy').squeeze().astype(np.int64)
    probe.xc = np.load(dir_path / 'xc.npy').squeeze()
    probe.yc = np.load(dir_path / 'yc.npy').squeeze()
    probe.kcoords = np.load(dir_path / 'kcoords.npy').squeeze()
    # Error checking
    assert (probe['chanMap'].shape == probe['xc'].shape == probe['yc'].shape == probe['kcoords'].shape ==
            (probe.NchanTOT - len(bad_channels),)), 'Inconsistent probe dimensions! '

    return probe


def make_bad_channels(path):
    if 'm043' in str(path):
        return (1, 15, 17)
    else:
        return (1,)


def clear_cache(dir_path):
    if (dir_path / '.kilosort').exists():
        print(f"Deleting cache files to start from scratch, as asked {(dir_path / '.kilosort')}")
        shutil.rmtree(dir_path / '.kilosort')
    elif (dir_path / '.phy').exists():
        print(f"Deleting cache files to start from scratch, as asked {(dir_path / '.phy')}")
        shutil.rmtree(dir_path / '.phy')
    if (dir_path / 'output').exists():
        print(f"Deleting cache files to start from scratch, as asked {(dir_path / '.output')}")
        shutil.rmtree(dir_path / 'output')
    # TODO maybe remove channel files, too
    # os.remove(dir_path / '*npy') if (dir_path ).glob('*.npy').exists() else None


def run_ksort(spikesorting_path='~/res/pyks/bucket_9_m043_1577205047_/',
              probe_type='trode',
              n_channels=32,
              bad_channels=None,
              chans_per_trode=2,
              do_force_rerun=False,
              params=None,
              sampling_rate=None,
              ):
    """
    absolute_path should contain data.dat
    from spikesort import run_spikesorting
    run_spikesorting.spikesorting.run_ksort('~/res/pyks/bucket_9_m043_1577205047_/', do_force_rerun=True)

    :param spikesorting_path:   Path to data.dat
    :param probe_type:      'trode' or 'shank'
    :param n_channels:
    :param bad_channels:    eg (1, 15,17). 1 is almost bad, since it's HPC
    :param chans_per_trode: 2 for stereotrode, 16 for cambridge probe
    :param do_force_rerun:  (optional) whether to clear cache, true or false
    :param params:          (optional) can enter params from scratch, see Make parameters section below
    :return:
    """
    """
    run_ksort(spikesorting_path=eu('~/res/pyks/bucket_9_m043_1577205047_/'), probe_type='trode', n_channels=32, do_force_rerun=True)
    As a starting point, ~/res/pyks/bucket_9_m043_1577205047_ only has data.dat
    """
    from pykilosort import run
    from pykilosort.params import KilosortParams
    # Check paths
    # spikesorting_path = eu('~/res/pyks/bucket_9_m043_1577205047_/output')
    original_working_directory = Path(os.curdir).absolute()
    print(f"Preserving original working directory {original_working_directory}")
    dir_path = Path(spikesorting_path).expanduser()
    print(f"Attempting to sort {dir_path / 'data.dat'}")
    assert dir_path.exists(), f'Make sure {dir_path} exists!'
    assert (dir_path / 'data.dat').exists(), f'Make sure data.dat is in {dir_path}'
    # Set paths for spikesorting
    # os.chdir(dir_path)
    dat_path = Path('data.dat')
    # dir_path = dat_path.parent

    # Delete cache files
    if do_force_rerun:
        clear_cache(dir_path)
    # Use per-mouse function to populate bad channels if not provided
    # This is better suited for a datajoint schema!
    if not bad_channels:
        bad_channels = make_bad_channels(spikesorting_path)

    # Make probe and write .npy files
    if probe_type == 'trode':
        # import pdb; pdb.set_trace()
        probe = make_trode_probe(dir_path, n_channels, bad_channels, chans_per_trode)
    else:
        raise NotImplementedError(f'Probe {probe_type} is not implemented!')
    print(f'Bad channels: {bad_channels}')
    print(probe)
    os.chdir(dir_path)
    dir_path = Path('.')

    # Make parameters
    if params is None:
        params = KilosortParams(
            # Some parameters probably worth tweaking:
            ThPre=8,  # (8 "threshold crossings for pre-clustering (in PCA projection space)")
            minFR=0.5,  # Half a hertz
            fs=3e4,
            fshigh=300.0,  # Higher low-pass filter than default 150Hz
            nfilt_factor=8,  # max number of clusters per good channel (even temporary ones) Default 4
            # spatial constant in um for computing residual variance of spike. Default 30
            sigmaMask=10,
            nt0=40,  # times around the peak to consider (Default 61)->37
            nskip=5,  # default 5 how many batches to skip for determining spike PCs
            ntbuff=48,  # samples of symmetrical buffer for whitening and spike detection Default 48
            # must be multiple of 32 + ntbuff. This is the batch size (try decreasing if out of memory).
            # danger, changing this setting can lead to fatal errors. options for determining PCs
            spkTh=-6,  # spike threshold in standard deviations (Default -6)
        )
    print(params)
    # TODO how do I set logging level properly?
    # add_default_handler(level='WARNING')  # Only progress bar
    # add_default_handler(level='INFO')  # more Progress messages
    # add_default_handler(level='DEBUG')  # All the messages
    print(probe)
    print(f'Running spikesorting on {spikesorting_path / dat_path}...')

    # allocate context to prevent errors during return
    # Run spikesorting
    try:
        ctx = run(dat_path, probe=probe, dir_path=dir_path, n_channels=probe.NchanTOT, dtype=np.int16, params=params,
                  sample_rate=params.fs)
    except RuntimeError:
        os.chdir(original_working_directory)
        return
    except Exception as e:
        os.chdir(original_working_directory)
        raise e
    os.chdir(original_working_directory)
    return ctx


def run_ksort_matlab(spikesorting_path='~/res/pyks/bucket_9_m043_1577205047_/',
                     probe_type='trode',
                     n_channels=32,
                     bad_channels=None,
                     chans_per_trode=2,
                     do_force_rerun=True,
                     params=None,
                     sampling_rate=30000,
                     ):
    raise NotImplementedError('Matlab sorting is not implemented')
