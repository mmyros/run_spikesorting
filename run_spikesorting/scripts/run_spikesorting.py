
# Install pykilosort first (see install_pykilosort.sh)

# Example command on all subfolders
# conda  activate pyks2;cd ~/res/pyks;python ~/mmy/mmypy/spikesort/run_spikesorting.py sort

# while sorting is going on, can start curation with PHY as soon as the first folder is done:
# conda  activate pyks2;cd ~/res/pyks;python ~/mmy/mmypy/spikesort/run_spikesorting.py curate

# OR on specific folder instead of all subfolders:
# conda activate pyks2;cd;python ~/mmy/mmypy/spikesort/run_spikesorting.py  sort -f /home/m/res/pyks/bucket_9_m043_1577205047_


# spikesorting.run_ksort('~/res/pyks/bucket_9_m043_1577205047_/', do_force_rerun=True)

import os
from os.path import expanduser as eu
from pathlib import Path

import click

from run_spikesorting import wrappers


@click.command()
@click.argument('action', default='all', type=str)
@click.option('-f', 'folder', default='.', type=click.Path(exists=True))
@click.option('-o', 'output_folder', default='~/', type=click.Path())
@click.option('-n', '--n-channels', default=32, type=int)
@click.option('-b', '--bad-channels', default='', type=str)
@click.option('-l', '--like', default='*', type=str)
def cli(action='sort', folder='.', output_folder='~/', like='*', **kwargs):
    print(folder, output_folder, action)

    if folder is None:
        folder = os.getcwd()
    folder = Path(eu(folder))
    if (folder / 'data.dat').exists() or (folder / 'data.bin').exists():
        wrappers.run_one_folder(folder=folder, action=action, output_folder=output_folder, **kwargs)
    else:
        print(f'Did not find data.dat or data.bin in {folder}. Trying subfolders..')
        wrappers.run_on_all_subdirectories(folder_with_data_subfolders=folder,
                                           action=action,
                                           like=like,
                                           output_folder=output_folder, **kwargs)


# Launch this file and drop into debug if needed
if __name__ == '__main__':
    try:
        cli()
    except SystemExit:
        pass
    except Exception as e:
        print('Error. Trying to start debugger... :\n ', e)
        import sys, traceback
        import pdb

        extype, value, tb = sys.exc_info()
        traceback.print_exc()
        pdb.post_mortem(tb)
