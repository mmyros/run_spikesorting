import os
from os.path import expanduser as eu
from pathlib import Path

from tqdm import tqdm

from . import file_conversion, spikesorting, curation


def run_one_folder(folder, action, output_folder=None, **kwargs):
    folder = Path(folder)

    output_folder = Path(output_folder)
    if not output_folder.exists():
        output_folder.expanduser().mkdir(exist_ok=True)

    if action == 'convert':
        print(f'Converting {folder} to datin {output_folder} in {output_folder}')

        file_conversion.convert_blackrock_to_kilosort(file_name=folder.stem, input_path=folder.parent,
                                                      kilosort_path=output_folder or '.',
                                                      **kwargs)
    elif action == 'sort':
        spikesorting.run_ksort(spikesorting_path=str(output_folder / folder.stem or '.'), **kwargs)

    elif action == 'convert_and_sort':
        print(f'Converting {folder} to dat in {output_folder} and sorting with kilosort')
        file_conversion.convert_blackrock_to_kilosort(file_name=folder.stem, input_path=folder.parent,
                                                      kilosort_path=output_folder or '.',
                                                      **kwargs)

        spikesorting.run_ksort(spikesorting_path=str(output_folder / folder.stem or '.'), **kwargs)

    elif action == 'curate':
        curation.run_phy(path=folder)

    elif action == 'all':
        print(f"Converting, sorting, and curating {folder.stem} to {folder.parent} to {output_folder}")
        file_conversion.convert_blackrock_to_kilosort(file_name=folder.stem, input_path=folder.parent,
                                                      kilosort_path=output_folder or '.',
                                                      **kwargs)

        spikesorting.run_ksort(spikesorting_path=str(output_folder / folder.stem or '.'), **kwargs)
        curation.run_phy(path=output_folder / folder.stem)

    else:
        raise NotImplementedError(f'Your argument to run_spikesorting was {action}. There is no such action')


def run_on_all_subdirectories(folder_with_data_subfolders=None, action='sort', like='*', **kwargs):
    # folder_with_data_subfolders='~/res/pyks'
    if folder_with_data_subfolders is None:
        folder_with_data_subfolders = os.getcwd()

    subfolders = Path(folder_with_data_subfolders).expanduser().glob(like)
    # print(list(subfolders))
    assert hasattr(subfolders, '__next__'), 'No subfolders! Are you in the right directory? '

    for subfolder in tqdm(list(subfolders), desc="Processing f"):
        print(f'Processing {subfolder}')
        run_one_folder(folder=subfolder, action=action, **kwargs)
        os.chdir(folder_with_data_subfolders)
