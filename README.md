[![pipeline status](https://gitlab.com/INsection/run_spikesorting/badges/master/pipeline.svg)](https://gitlab.com/INsection/run_spikesorting/-/commits/master)


[![coverage report](https://gitlab.com/INsection/run_spikesorting/badges/master/coverage.svg)](https://gitlab.com/INsection/run_spikesorting/-/commits/master)

Note: Coverage = 69% when using all tests on an environment with CUDA

# run_spikesorting

Workflow to convert and sort extracellular ephys data

# Setup
Step 0:
Install CUDA for your operating system. For Windows, cupy currently seems to only work with version 11.0.2. Windows links 

https://developer.nvidia.com/cuda-11.0-download-archive?target_os=Windows&target_arch=x86_64&target_version=10&target_type=exenetwork

Step 2: Reboot

Steps 2-4: 
In Anaconda Powershell prompt/WSL prompt (Windows) or Terminal (Mac/Linux):
```bash
# 2. Clone/download this repository

# 3. Install conda dependencies:
pip install -r requirements.txt

# 3.1. Install cupy (See below if this fails)
pip install cupy-cuda110

# 4. install run_spikesorting, pykilosort (automatically), etc
pip install -e .
```

## Setup troubleshooting
### If cupy throws something like 'no atttribute core'
(or a different version see https://cupy.chainer.org/)
(check cuda version cudaXXX from `nvcc --version`)
```
pip install cupy-cuda110==8.5.0
```


# Usage from command line (Recommended)

```bash
cd my/data/
run_spikesorting convert
OR 
run_spikesorting sort
OR 
run_spikesorting curate

-f option can be used to specify folder:
run_spikesorting sort -f my/data
```
# Usage from python/Spyder/PyCharm:
```python
from run_spikesorting import wrappers
path='C:\my_path_to_files'
wrappers.run_one_folder(path, 'sort')
wrappers.run_on_all_subdirectories(path, 'sort')
```
OR
```python
import run_spikesorting
input_path='C:\my_path_to_files'
file_name='my_file.ns6'
params=dict()
run_spikesorting.file_conversion.convert_blackrock_to_kilosort(file_name, input_path)
run_spikesorting.spikesorting.run_ksort(input_path, params=params)
run_spikesorting.curation.run_phy(input_path)
```
See respective files for more options

